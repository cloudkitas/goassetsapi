package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"ekuycapital.xyz/assetsapi/cmd/web/handlers"
	"github.com/gorilla/mux"
)

func main() {

	l := log.New(os.Stdout, "Assets-API", log.LstdFlags)
	ah := handlers.NewAssets(l)
	sm := mux.NewRouter()

	getRoute := sm.Methods(http.MethodGet).Subrouter()
	getRoute.HandleFunc("/", ah.GetAssets)

	apiGetRoute := sm.Methods(http.MethodGet).Subrouter()
	apiGetRoute.HandleFunc("/api/assets", ah.GetAssetsAPI)

	addRoute := sm.Methods(http.MethodPost).Subrouter()
	addRoute.HandleFunc("/asset", ah.AddAsset)
	addRoute.Use(ah.MiddlewareValidateAsset)

	updateRoute := sm.Methods(http.MethodPut).Subrouter()
	updateRoute.HandleFunc("/assets/{id:[0-9]+}", ah.UpdateAsset)
	updateRoute.Use(ah.MiddlewareValidateAsset)

	fmt.Println("Hello")
	getVal := reverseString("bk")
	fmt.Println(getVal)

	s := http.Server{
		Addr:         ":9090",
		Handler:      sm,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  100 * time.Second,
		WriteTimeout: 100 * time.Second,
	}

	go func() {
		err := s.ListenAndServe()
		if err != nil {
			log.Fatalln(err.Error())
		}
	}()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)
	signal.Notify(sig, os.Kill)

	sigChan := <-sig
	log.Println("Notified to shut f down", sigChan)

	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(ctx)

}

func reverseString(s string) int {

	letters := []string{}

	leng := len(s)

	for i, l := range letters {
		if leng == 2 {
			return i
			fmt.Println(l)
		}

	}
	return -1

}
