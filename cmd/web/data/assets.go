package data

import (
	"encoding/json"
	"fmt"
	"io"
)

type Asset struct {
	ID       int
	Name     string
	Price    float64
	Holdings float64
}

type assets []*Asset

func (a *Asset) FromJSON(r io.Reader) error {
	d := json.NewDecoder(r)
	return d.Decode(a)
}

func GetAssets() assets {
	return AssetsList
}

func (a *assets) ToJSON(w io.Writer) error {
	e := json.NewEncoder(w)
	return e.Encode(a)
}

func AddAsset(a *Asset) {
	a.ID = getNextAssetID()
	AssetsList = append(AssetsList, a)
}

func UpdateAsset(id int, a *Asset) error {
	pos, _, err := findAsset(id)
	if err != nil {
		return err
	}

	a.ID = id
	AssetsList[pos] = a
	return nil
}

var ErrAssetNotFound = fmt.Errorf("Asset Not Found Error ")

func findAsset(id int) (int, *Asset, error) {
	for i, a := range AssetsList {
		if id == a.ID {
			return i, a, nil
		}
	}

	return -1, nil, ErrAssetNotFound

}

func getNextAssetID() int {
	la := AssetsList[len(AssetsList)-1]
	return la.ID + 1
}

var AssetsList = []*Asset{
	&Asset{
		ID:       1,
		Name:     "BTC",
		Price:    30000,
		Holdings: 34,
	},
	&Asset{
		ID:       2,
		Name:     "ETH",
		Price:    3232,
		Holdings: 12,
	},
}
