package handlers

import (
	"net/http"

	"ekuycapital.xyz/assetsapi/cmd/web/data"
)

func (a *Assets) GetAssetsAPI(rw http.ResponseWriter, r *http.Request) {
	a.l.Println("Handle Assets API GET Request ")
	asslis := data.GetAssets()
	err := asslis.ToJSON(rw)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusBadRequest)
		return
	}
}
