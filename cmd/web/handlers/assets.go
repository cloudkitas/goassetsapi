package handlers

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"ekuycapital.xyz/assetsapi/cmd/web/data"
	"github.com/gorilla/mux"
)

type Assets struct {
	l *log.Logger
}

func NewAssets(l *log.Logger) *Assets {
	return &Assets{l}
}

func (a *Assets) GetAssets(rw http.ResponseWriter, r *http.Request) {
	a.l.Println("Handle Get Method")

	files := []string{
		"./cmd/web/ui/html/get.page.gotmpl",
		"./cmd/web/ui/html/base.template.gotmpl",
	}

	tpl, err := template.ParseFiles(files...)
	if err != nil {
		http.Error(rw, "File load error", http.StatusInternalServerError)
		fmt.Println(err.Error())
		return
	}

	al := data.GetAssets()

	//err = al.ToJSON(rw)
	/*
		if err != nil {
			http.Error(rw, "Unable to marshal json", http.StatusBadRequest)
			return
		}
	*/

	tpl.Execute(rw, al)

}

func (a *Assets) AddAsset(rw http.ResponseWriter, r *http.Request) {
	asst := r.Context().Value(keyAsset{}).(data.Asset)
	data.AddAsset(&asst)
	fmt.Fprint(rw, "requested to add asset", asst.ID)

}

func (a Assets) UpdateAsset(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(rw, "Unable to convert ID", http.StatusBadRequest)
		return
	}

	a.l.Println("Handle Update Asset with Id, ", id)

	asst := r.Context().Value(keyAsset{}).(data.Asset)
	err = data.UpdateAsset(id, &asst)

	if err == data.ErrAssetNotFound {
		http.Error(rw, "Unable to Find Asset for real", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, "Unable to update for real", http.StatusInternalServerError)
		return
	}

}

type keyAsset struct{}

func (a Assets) MiddlewareValidateAsset(next http.Handler) http.Handler {
	return http.HandlerFunc((func(rw http.ResponseWriter, r *http.Request) {
		asst := data.Asset{}
		err := asst.FromJSON(r.Body)
		if err != nil {
			http.Error(rw, "Unable to marshalsllll json", http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), keyAsset{}, asst)
		r = r.WithContext(ctx)

		next.ServeHTTP(rw, r)
	}))
}
